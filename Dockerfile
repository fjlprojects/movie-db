FROM amazoncorretto:11-alpine-jdk
COPY target/movie-db-0.0.1.jar movie-db-0.0.1.jar
ENTRYPOINT ["java","-jar","/movie-db-0.0.1.jar"]