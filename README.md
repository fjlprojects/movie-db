# README #

Tech challenge for Rindus.

### Requirements ###
Create a REST-based microservice to handle a movie database.

* The only compulsory fields for each movie are `title` and `year`. It can have more fields if it helps with your design.
* There is no need to handle authentication or authorization for this microservice.
* There must be an endpoint allowing to create a new movie entry
* The movie `title` must be unique
* There must be an endpoint that lists existing movies, with an optional query parameter filtering them by year.

The internal microservice will be running on kubernetes within an internal secure network.
Our build pipelines handles the configuration parts. As an only requirement we need a working
Docker configuration file in the base directory.

For data persistence we use the data store which is most suitable for the job.

You can use any language and library of your choice.

The service will be running in production. Make sure it is prepared for it and we know what it is doing.
Feel free to share any information that helps us to follow and understand your solution.

Submit your solution as an archived git repository.


### Language ###

Java 11 has been selected for this app. It has been developed using IntelliJ in a MacOS environment.

### Database ###

Application has been configured to work with a MongoDB database. A docker image for mongo has been used along development. 
In order to reproduce this environment is necessary to have Docker installed and use this command:
docker run -d -p 27017:27017 --name databasename mongo:4.2


### Dockerization ###

Application has been dockerized using Corretto distribution from Amazon.