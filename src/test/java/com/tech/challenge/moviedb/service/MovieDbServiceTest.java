package com.tech.challenge.moviedb.service;

import com.tech.challenge.moviedb.dao.MovieEntity;
import com.tech.challenge.moviedb.dao.MovieRepository;
import com.tech.challenge.moviedb.domain.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class MovieDbServiceTest {

    @Mock
    private final MovieRepository movieRepository = Mockito.mock(MovieRepository.class);

    private MovieDbService movieDbService;

    @BeforeEach
    void setUp() {
        movieDbService = new MovieDbService(movieRepository);
    }

    @Test
    void when_year_is_sent_then_has_to_call_find_by_year() {
        // GIVEN
        Integer year = 2021;
        when(movieRepository.findByYear(year)).thenReturn(new ArrayList<>());

        // WHEN
        movieDbService.fetchMovies(year);

        // THEN
        verify(movieRepository, times(1)).findByYear(year);
    }

    @Test
    void when_no_year_is_sent_then_has_to_call_find_all() {
        // GIVEN
        when(movieRepository.findAll()).thenReturn(new ArrayList<>());

        // WHEN
        movieDbService.fetchMovies(null);

        // THEN
        verify(movieRepository, times(1)).findAll();
    }

    @Test
    void when_fetch_for_movies_then_has_to_return_a_list_of_movie() {
        // GIVEN
        List<MovieEntity> movieEntityList = new ArrayList<>();
        MovieEntity movie1 = new MovieEntity();
        movie1.setTitle("Star Wars");
        movie1.setYear(1977);

        MovieEntity movie2 = new MovieEntity();
        movie2.setTitle("Matrix 4");
        movie2.setYear(2021);

        movieEntityList.add(movie1);
        movieEntityList.add(movie2);

        when(movieRepository.findAll()).thenReturn(movieEntityList);

        // WHEN
        List<Movie> movieList = movieDbService.fetchMovies(null);

        // THEN
        assertThat(movieList).isNotNull();
        assertThat(movieList.size()).isEqualTo(2);
    }

    @ParameterizedTest
    @MethodSource("provideValuesForExist")
    void existTitleTest(MovieEntity movie, boolean expectedResult) {
        // GIVEN
        String title = "A title";
        when(movieRepository.findByTitle(title)).thenReturn(movie);

        // WHEN
        boolean exists = movieDbService.existTitle(title);

        // THEN
        assertThat(exists).isEqualTo(expectedResult);
    }

    @Test
    void when_create_then_has_to_save_and_return_new_movie() {
        // GIVEN
        Movie movie = new Movie();
        movie.setTitle("Star Wars");
        movie.setYear(1977);

        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setTitle("Star Wars");
        movieEntity.setYear(1977);

        when(movieRepository.save(Mockito.any(MovieEntity.class))).thenReturn(movieEntity);

        // WHEN
        Movie movieResult = movieDbService.createMovie(movie);

        // THEN
        assertThat(movieResult.getTitle()).isEqualTo(movie.getTitle());
        assertThat(movieResult.getYear()).isEqualTo(movie.getYear());
    }

    private static List<Arguments> provideValuesForExist() {
        List<Arguments> arguments = new ArrayList<>();
        MovieEntity movie1 = new MovieEntity();
        movie1.setTitle("Star Wars");
        movie1.setYear(1977);

        arguments.add(Arguments.of(movie1, true));
        arguments.add(Arguments.of(null, false));

        return arguments;
    }
}