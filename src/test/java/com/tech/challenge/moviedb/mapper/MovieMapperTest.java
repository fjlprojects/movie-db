package com.tech.challenge.moviedb.mapper;

import com.tech.challenge.moviedb.dao.MovieEntity;
import com.tech.challenge.moviedb.domain.Movie;
import com.tech.challenge.moviedb.dto.MovieDbRequest;
import com.tech.challenge.moviedb.dto.MovieDbResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MovieMapperTest {

    private Movie movie;
    private MovieEntity movieEntity;
    private MovieDbRequest movieDbRequest;
    private MovieDbResponse movieDbResponse;

    @BeforeEach
    void setUp() {
        movie = new Movie();
        movie.setTitle("Star Wars");
        movie.setYear(1977);

        movieEntity = new MovieEntity();
        movieEntity.setTitle("Star Wars");
        movieEntity.setYear(1977);

        movieDbRequest = new MovieDbRequest();
        movieDbRequest.setTitle("Star Wars");
        movieDbRequest.setYear(1977);

        movieDbResponse = new MovieDbResponse();
        movieDbResponse.setTitle("Star Wars");
        movieDbResponse.setYear(1977);
    }

    @Test
    void toDto() {
        // WHEN
        MovieDbResponse response = MovieMapper.toDto(movie);

        // THEN
        assertThat(response.getTitle()).isEqualTo(movieDbResponse.getTitle());
        assertThat(response.getYear()).isEqualTo(movieDbResponse.getYear());
    }

    @Test
    void fromDto() {
        // WHEN
        Movie response = MovieMapper.fromDto(movieDbRequest);

        // THEN
        assertThat(response.getTitle()).isEqualTo(movie.getTitle());
        assertThat(response.getYear()).isEqualTo(movie.getYear());
    }

    @Test
    void toEntity() {
        // WHEN
        MovieEntity response = MovieMapper.toEntity(movie);

        // THEN
        assertThat(response.getTitle()).isEqualTo(movieEntity.getTitle());
        assertThat(response.getYear()).isEqualTo(movieEntity.getYear());
    }

    @Test
    void fromEntity() {
        // WHEN
        Movie response = MovieMapper.fromEntity(movieEntity);

        // THEN
        assertThat(response.getTitle()).isEqualTo(movie.getTitle());
        assertThat(response.getYear()).isEqualTo(movie.getYear());
    }
}