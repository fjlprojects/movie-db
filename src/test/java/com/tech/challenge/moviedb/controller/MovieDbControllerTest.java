package com.tech.challenge.moviedb.controller;

import com.tech.challenge.moviedb.domain.Movie;
import com.tech.challenge.moviedb.service.MovieDbService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class MovieDbControllerTest {

    @Mock
    private final MovieDbService movieDbService = Mockito.mock(MovieDbService.class);

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MovieDbController movieDbController = new MovieDbController(movieDbService);
        this.mockMvc = MockMvcBuilders.standaloneSetup(movieDbController).build();
    }

    @ParameterizedTest
    @MethodSource("provideGetMoviesTestData")
    void when_get_movies_then_has_to_return_an_array_of_movies(List<Movie> result, String expectedResult) throws Exception {
        // GIVEN
        String controllerPath = "/movies";

        when(movieDbService.fetchMovies(null)).thenReturn(result);

        // WHEN
        ResultActions response = mockMvc.perform(get(controllerPath));

        // THEN
        response.andExpect(status().isOk());
        response.andExpect(content().json(expectedResult));
        verify(movieDbService, times(1)).fetchMovies(null);
    }

    @ParameterizedTest
    @MethodSource("provideCreateMovieTestData")
    void when_post_movies_then_has_to_create_a_movie_or_return_an_error_message(
            Movie movie, String body, boolean exists, String expectedResult, int expectedStatus) throws Exception {
        // GIVEN
        String controllerPath = "/movies";

        when(movieDbService.createMovie(Mockito.any(Movie.class))).thenReturn(movie);
        when(movieDbService.existTitle(Mockito.any(String.class))).thenReturn(exists);

        // WHEN
        ResultActions response = mockMvc.perform(post(controllerPath)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(body)
                .accept(MediaType.APPLICATION_JSON_VALUE));

        // THEN
        response.andExpect(status().is(expectedStatus));
        response.andExpect(content().json(expectedResult));
    }

    private static List<Arguments> provideGetMoviesTestData() {
        List<Arguments> arguments = new ArrayList<>();
        Movie movie1 = new Movie();
        movie1.setTitle("Star Wars");
        movie1.setYear(1977);

        Movie movie2 = new Movie();
        movie2.setTitle("Matrix 4");
        movie2.setYear(2021);

        List<Movie> result1 = new ArrayList<>();
        String expected1 = "[]";

        List<Movie> result2 = new ArrayList<>();
        result2.add(movie1);
        String expected2 = "[{\"title\":\"Star Wars\", \"year\":1977}]";

        List<Movie> result3 = new ArrayList<>();
        result3.add(movie1);
        result3.add(movie2);
        String expected3 = "[{\"title\":\"Star Wars\", \"year\":1977},{\"title\":\"Matrix 4\", \"year\":2021}]";

        arguments.add(Arguments.of(result1, expected1));
        arguments.add(Arguments.of(result2, expected2));
        arguments.add(Arguments.of(result3, expected3));

        return arguments;
    }

    private static List<Arguments> provideCreateMovieTestData() {
        List<Arguments> arguments = new ArrayList<>();
        Movie movie1 = new Movie();
        movie1.setTitle("Star Wars");
        movie1.setYear(1977);

        Movie movie2 = new Movie();
        movie2.setTitle("Matrix 5");
        movie2.setYear(2050);

        Movie movie3 = new Movie();
        movie3.setTitle("");
        movie3.setYear(2021);

        String body1 = "{\"title\":\"Star Wars\", \"year\":1977}";
        String body2 = "{\"title\":\"Matrix 5\", \"year\":2050}]";
        String body3 = "{\"title\":\"\", \"year\":2021}]";

        String expected1 = "{\"title\":\"Star Wars\", \"year\":1977, \"message\":\"CREATED\"}";
        String expected1_1 = "{\"title\":\"Star Wars\", \"year\":1977, \"message\":\"ERRORS::  : Title already exists\"}";
        String expected2 = "{\"title\":\"Matrix 5\", \"year\":2050, \"message\":\"ERRORS::  : Invalid year\"}]";
        String expected3 = "{\"title\":\"\", \"year\":2021, \"message\":\"ERRORS::  : Title have to be not blank\"}]";

        arguments.add(Arguments.of(movie1, body1, false, expected1, HttpStatus.OK.value()));
        arguments.add(Arguments.of(movie1, body1, true,  expected1_1, HttpStatus.BAD_REQUEST.value()));
        arguments.add(Arguments.of(movie2, body2, false, expected2, HttpStatus.BAD_REQUEST.value()));
        arguments.add(Arguments.of(movie3, body3, false, expected3, HttpStatus.BAD_REQUEST.value()));

        return arguments;
    }

}