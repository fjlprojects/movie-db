package com.tech.challenge.moviedb.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
@ExtendWith(SpringExtension.class)
class MovieRepositoryTest {

    @Autowired
    MovieRepository repository;

    MovieEntity movie1, movie2, movie3;

    @BeforeEach
    public void setUp() {
        repository.deleteAll();

        movie1 = new MovieEntity();
        movie1.setTitle("Star Wars");
        movie1.setYear(1977);

        movie2 = new MovieEntity();
        movie2.setTitle("Matrix 4");
        movie2.setYear(2021);

        movie3 = new MovieEntity();
        movie3.setTitle("Black Widow");
        movie3.setYear(2021);

        movie1 = repository.save(movie1);
        movie2 = repository.save(movie2);
        movie3 = repository.save(movie3);
    }

    @Test
    public void setsIdOnSave() {
        // GIVEN
        MovieEntity movie = new MovieEntity();
        movie.setTitle("New Movie");
        movie.setYear(2021);

        // WHEN
        MovieEntity savedMovie = repository.save(movie);

        // THEN
        assertThat(savedMovie.getId()).isNotNull();
    }

    @Test
    public void findByYear() {
        // WHEN
        List<MovieEntity> result = repository.findByYear(2021);

        // THEN
        assertThat(result).hasSize(2).extracting("title").contains("Matrix 4", "Black Widow");
    }

    @Test
    public void findsByTitle() {
        // GIVEN
        MovieEntity sw = new MovieEntity();
        sw.setTitle("Star Wars");

        // WHEN
        List<MovieEntity> result = repository.findAll(Example.of(sw));

        // THEN
        assertThat(result).hasSize(1).extracting("year").contains(1977);
    }
}