package com.tech.challenge.moviedb.mapper;

import com.tech.challenge.moviedb.dao.MovieEntity;
import com.tech.challenge.moviedb.domain.Movie;
import com.tech.challenge.moviedb.dto.MovieDbRequest;
import com.tech.challenge.moviedb.dto.MovieDbResponse;

public class MovieMapper {

    public static MovieDbResponse toDto(Movie movie) {
        MovieDbResponse movieDbResponse = new MovieDbResponse();

        movieDbResponse.setTitle(movie.getTitle());
        movieDbResponse.setYear(movie.getYear());

        return movieDbResponse;
    }

    public static Movie fromDto(MovieDbRequest movieDto) {
        Movie movie = new Movie();

        movie.setTitle(movieDto.getTitle());
        movie.setYear((movieDto.getYear()));

        return movie;
    }

    public static MovieEntity toEntity(Movie movie) {
        MovieEntity movieEntity = new MovieEntity();

        movieEntity.setTitle(movie.getTitle());
        movieEntity.setYear(movie.getYear());

        return movieEntity;
    }

    public static Movie fromEntity(MovieEntity movieEntity) {
        Movie movie = new Movie();

        movie.setTitle(movieEntity.getTitle());
        movie.setYear((movieEntity.getYear()));

        return movie;
    }

}
