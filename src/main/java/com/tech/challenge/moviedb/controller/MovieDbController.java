package com.tech.challenge.moviedb.controller;

import com.tech.challenge.moviedb.domain.Movie;
import com.tech.challenge.moviedb.dto.MovieDbRequest;
import com.tech.challenge.moviedb.dto.MovieDbResponse;
import com.tech.challenge.moviedb.mapper.MovieMapper;
import com.tech.challenge.moviedb.service.MovieDbService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MovieDbController {


    private final MovieDbService movieDbService;

    public MovieDbController(MovieDbService movieDbService) {
        this.movieDbService = movieDbService;
    }

    @GetMapping("/movies")
    public ResponseEntity<List<MovieDbResponse>> getMovies(@RequestParam(required = false) Integer year) {
        List<Movie> movieList = movieDbService.fetchMovies(year);
        List<MovieDbResponse> results = movieList.stream().map(MovieMapper::toDto).collect(Collectors.toList());

        return ResponseEntity.ok(results);
    }

    @PostMapping("/movies")
    public ResponseEntity<MovieDbResponse> createMovie(@RequestBody MovieDbRequest movieDbRequest) {
        List<String> errors = new ArrayList<>();

        if (checkRequest(movieDbRequest,errors)) {
            Movie movie = MovieMapper.fromDto(movieDbRequest);
            MovieDbResponse createdMovie = MovieMapper.toDto(movieDbService.createMovie(movie));
            createdMovie.setMessage("CREATED");

            return ResponseEntity.ok(createdMovie);
        }

        MovieDbResponse errorResponse = new MovieDbResponse();
        errorResponse.setTitle(movieDbRequest.getTitle());
        errorResponse.setYear(movieDbRequest.getYear());
        StringBuilder message = new StringBuilder("ERRORS:: ");
        for (String error: errors)
            message.append(" : ").append(error);
        errorResponse.setMessage(message.toString());

        return ResponseEntity.badRequest().body(errorResponse);
    }

    private boolean checkRequest(MovieDbRequest movieDbRequest, List<String> errors) {
        boolean titleOk = false;
        boolean yearOk = false;
        if (StringUtils.isNotBlank(movieDbRequest.getTitle())) {
            if (movieDbService.existTitle(movieDbRequest.getTitle())) {
                errors.add("Title already exists");
            } else {
                titleOk = true;
            }
        } else {
           errors.add("Title have to be not blank");
        }

        if (movieDbRequest.getYear() != null && movieDbRequest.getYear().compareTo(LocalDate.now().getYear()) <= 0)
            yearOk = true;
        else {
            errors.add("Invalid year");
        }

        return titleOk && yearOk;
    }
}
