package com.tech.challenge.moviedb.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MovieRepository extends MongoRepository<MovieEntity, String> {
    public MovieEntity findByTitle(String title);
    public List<MovieEntity> findByYear(Integer year);
}
