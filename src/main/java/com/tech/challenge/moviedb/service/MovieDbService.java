package com.tech.challenge.moviedb.service;

import com.tech.challenge.moviedb.dao.MovieEntity;
import com.tech.challenge.moviedb.dao.MovieRepository;
import com.tech.challenge.moviedb.domain.Movie;
import com.tech.challenge.moviedb.mapper.MovieMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieDbService {

    private final MovieRepository movieRepository;

    public MovieDbService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public List<Movie> fetchMovies(Integer year) {
        List<MovieEntity> movieList;
        if (year != null) {
            movieList = movieRepository.findByYear(year);
        } else {
            movieList = movieRepository.findAll();
        }

        return movieList.stream().map(MovieMapper::fromEntity).collect(Collectors.toList());
    }

    public boolean existTitle(String title) {
        MovieEntity movieEntity = movieRepository.findByTitle(title);

        return movieEntity != null;
    }

    public Movie createMovie(Movie movie) {
        MovieEntity movieEntity = MovieMapper.toEntity(movie);
        movieRepository.save(movieEntity);

        return MovieMapper.fromEntity(movieEntity);
    }

}
